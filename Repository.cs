﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static PerformanceBulkAndParallel.DataTableForBulkInsert;

namespace PerformanceBulkAndParallel
{
    public  class Repository
    {
        private readonly string _connectionString;

        public Repository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable dataTable)
        {
            var destinoTabela = "tbConsulta";

            using (var dbConnection = new SqlConnection(_connectionString))
            {
                dbConnection.Open();

                var sqlCommand = $"IF (OBJECT_ID('##{destinoTabela}') IS NOT NULL) BEGIN DROP TABLE #{destinoTabela} END {Environment.NewLine}" +
                    $"CREATE TABLE ##{destinoTabela} (Cod VARCHAR(MAX) NOT NULL, Descricao varchar(max) NULL)  {Environment.NewLine}" +
                    $"TRUNCATE TABLE ##{destinoTabela}";
                var cmd = new SqlCommand(sqlCommand, dbConnection);
                cmd.ExecuteNonQuery();


                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null))
                {
                    s.BulkCopyTimeout = 360;
                    s.DestinationTableName = $"##{destinoTabela}";

                    var colunasDePara = new List<ColunaDePara>();
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Id", NomeColunaTabela = "Cod" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Description", NomeColunaTabela = "Descricao" });

                    foreach (var item in colunasDePara)
                        s.ColumnMappings.Add(item.NomeColunaDataTable, item.NomeColunaTabela);

                    s.WriteToServer(dataTable);
                }
            }
        }


        public ResultMessage MergeTempTableRecordsToMainTable()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                var result = conn.Query<ResultMessage>
                (
                    "sp_MergeTempTableRecordsToMainTable",
                    new
                    {

                    },
                    commandType: System.Data.CommandType.StoredProcedure
                );

                return result.FirstOrDefault();
            }
        }
    }
}
