﻿using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Threading.Tasks;

namespace PerformanceBulkAndParallel
{
    public class DataTableForBulkInsert
    {
        public static DataTable CreateDataTable()
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Description", typeof(string)));

            return dataTable;
        }
        public class ColunaDePara
        {
            public string NomeColunaTabela { get; set; }
            public string NomeColunaDataTable { get; set; }
        }

        public static void MergeListInDataTable(DataTable dataTable, IEnumerable<Record> recordList)
        {
            Parallel.ForEach(
                recordList,
                (record) =>
                {
                    lock (dataTable)
                    {
                        dataTable.Rows.Add(
                            record.Id,
                            record.Description
                        );
                    }
                });
        }

        public static DataTable ToDataTable<T>(IEnumerable<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}
